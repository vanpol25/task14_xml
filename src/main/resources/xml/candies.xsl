<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Candies table</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Energy</th>
                        <th>Stuffing</th>
                        <th>Ingredients</th>
                        <th>Proteins</th>
                        <th>Fats</th>
                        <th>Carbohydrates</th>
                        <th>Production</th>
                    </tr>
                    <xsl:for-each select="candies/candy">
                        <xsl:sort select="energy"/>
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="energy"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td>
                                <xsl:for-each select="ingredients">
                                    <xsl:value-of select="ingredient"/>
                                </xsl:for-each>
                            </td>
                            <td><xsl:value-of select="value/proteins"/></td>
                            <td><xsl:value-of select="value/fats"/></td>
                            <td><xsl:value-of select="value/carbohydrates"/></td>
                            <td><xsl:value-of select="production"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>