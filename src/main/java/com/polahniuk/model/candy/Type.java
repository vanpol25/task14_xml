package com.polahniuk.model.candy;

import lombok.Data;

/**
 * Class has information about stuffing in candy.
 */
@Data
public class Type {

    private String stuffing;
    private boolean include;

    @Override
    public String toString() {
        return "Type{" +
                "stuffing='" + stuffing + '\'' +
                ", include=" + include +
                '}';
    }
}