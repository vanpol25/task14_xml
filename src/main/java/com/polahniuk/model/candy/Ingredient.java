package com.polahniuk.model.candy;

/**
 * Enumeration class Ingredient.
 */
public enum Ingredient {

    Chocolate("Chocolate"),
    Sugar("Sugar"),
    Sweetener("Sweetener"),
    DairyProduct("Dairy product"),
    Nuts("Nuts"),
    MilkBased("Milk-based");

    /**
     * Name as in xmk document.
     */
    private String name;

    Ingredient(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * @param name - name in xml.
     * @return - Ingredient object by name.
     */
    public static Ingredient getIngredient(String name) {
        Ingredient[] values = Ingredient.values();
        Ingredient ingredient = null;

        for (Ingredient i : values) {
            if (i.name.equals(name)) {
                ingredient = i;
                break;
            }
        }
        return ingredient;
    }

    @Override
    public String toString() {
        return name;
    }
}