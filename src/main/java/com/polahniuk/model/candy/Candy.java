package com.polahniuk.model.candy;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Class candy.
 */
@Data
public class Candy {

    private String name;
    private int energy;
    private Type stuffing;
    private List<Ingredient> ingredient;
    private Value value;
    private String production;

    public Candy() {
        ingredient = new ArrayList<>();
    }

    public void addIngredient(Ingredient ingredient) {
        this.ingredient.add(ingredient);
    }

    @Override
    public String toString() {
        return "\nCandy{" +
                "name='" + name + '\'' +
                ", energy=" + energy +
                ", stuffing=" + stuffing +
                ", ingredient=" + ingredient +
                ", value=" + value +
                ", production='" + production + '\'' +
                '}';
    }
}