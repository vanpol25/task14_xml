package com.polahniuk.model.parser;

import com.polahniuk.model.candy.Candy;
import com.polahniuk.model.candy.Ingredient;
import com.polahniuk.model.candy.Type;
import com.polahniuk.model.candy.Value;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which parse xml documents using StAX.
 */
public class StaxParser {

    private XMLInputFactory xmlInputFactory;
    private XMLEventReader reader;
    private List<Candy> candies = new ArrayList<>();

    public StaxParser(File path) throws FileNotFoundException, XMLStreamException {
        xmlInputFactory = XMLInputFactory.newInstance();
        reader = xmlInputFactory.createXMLEventReader(new FileInputStream(path));
        parse();
    }

    public List<Candy> getCandies() {
        return candies;
    }

    private void parse() throws XMLStreamException {
        Candy candy = null;
        Type type = null;
        Value value = null;
        XMLEvent nextEvent;

        while (reader.hasNext()) {
            nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement element = nextEvent.asStartElement();
                if (element.getName().getLocalPart().equals("candy")) {
                    candy = new Candy();
                } else if (element.getName().getLocalPart().equals("name")) {
                    nextEvent = reader.nextEvent();
                    candy.setName(nextEvent.asCharacters().getData());
                } else if (element.getName().getLocalPart().equals("energy")) {
                    nextEvent = reader.nextEvent();
                    candy.setEnergy(Integer.valueOf(nextEvent.asCharacters().getData()));
                } else if (element.getName().getLocalPart().equals("type")) {
                    type = new Type();
                    Attribute stuffing = element.getAttributeByName(new QName("stuffing"));
                    if (stuffing != null) {
                        type.setInclude(Boolean.valueOf(stuffing.getValue()));
                    }
                    nextEvent = reader.nextEvent();
                    type.setStuffing(nextEvent.asCharacters().getData());
                } else if (element.getName().getLocalPart().equals("ingredient")) {
                    nextEvent = reader.nextEvent();
                    Ingredient ingredient = Ingredient.getIngredient(nextEvent.asCharacters().getData());
                    candy.addIngredient(ingredient);
                } else if (element.getName().getLocalPart().equals("value")) {
                    value = new Value();
                } else if (element.getName().getLocalPart().equals("proteins")) {
                    nextEvent = reader.nextEvent();
                    value.setProteins(Double.valueOf(nextEvent.asCharacters().getData()));
                } else if (element.getName().getLocalPart().equals("fats")) {
                    nextEvent = reader.nextEvent();
                    value.setFats(Double.valueOf(nextEvent.asCharacters().getData()));
                } else if (element.getName().getLocalPart().equals("carbohydrates")) {
                    nextEvent = reader.nextEvent();
                    value.setCarbohydrates(Double.valueOf(nextEvent.asCharacters().getData()));
                } else if (element.getName().getLocalPart().equals("production")) {
                    nextEvent = reader.nextEvent();
                    candy.setProduction(nextEvent.asCharacters().getData());
                }
            } else if (nextEvent.isEndElement()) {
                EndElement element = nextEvent.asEndElement();
                if (element.getName().getLocalPart().equals("candy")) {
                    candies.add(candy);
                } else if (element.getName().getLocalPart().equals("type")) {
                    candy.setStuffing(type);
                } else if (element.getName().getLocalPart().equals("value")) {
                    candy.setValue(value);
                }
            }
        }
    }
}
