package com.polahniuk.model.parser;

import com.polahniuk.model.candy.Candy;
import com.polahniuk.model.candy.Ingredient;
import com.polahniuk.model.candy.Type;
import com.polahniuk.model.candy.Value;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which parse xml documents using DOM.
 */
public class DomParser {

    private Logger log = LogManager.getLogger(DomParser.class);

    private List<Candy> candies = new ArrayList<>();

    /**
     * @param file - file of xml.
     */
    public DomParser(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);
        NodeList nodeList = document.getElementsByTagName("candy");
        findCandies(nodeList);
    }

    public List<Candy> getCandies() {
        return candies;
    }

    private void findCandies(NodeList nodeList) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Candy candy = new Candy();
            NodeList childNodes = nodeList.item(i).getChildNodes();
            for (int j = 0; j < childNodes.getLength(); j++) {
                Node childItem = childNodes.item(j);
                if (childItem.getNodeName().equals("name")) {
                    candy.setName(childItem.getTextContent());
                } else if (childItem.getNodeName().equals("energy")) {
                    candy.setEnergy(Integer.valueOf(childItem.getTextContent()));
                } else if (childItem.getNodeName().equals("type")) {
                    candy.setStuffing(getType(childItem));
                } else if (childItem.getNodeName().equals("ingredients")) {
                    candy.setIngredient(getIngredients(childItem));
                } else if (childItem.getNodeName().equals("value")) {
                    candy.setValue(getValue(childItem));
                } else if (childItem.getNodeName().equals("production")) {
                    candy.setProduction(childItem.getTextContent());
                }
            }
            candies.add(candy);
        }
    }

    private Type getType(Node node) {
        Type type = new Type();
        String include = node.getAttributes().item(0).getTextContent();
        String stuffing = node.getTextContent();
        type.setInclude(Boolean.valueOf(include));
        type.setStuffing(stuffing);
        return type;
    }

    private List<Ingredient> getIngredients(Node node) {
        List<Ingredient> ingredients = new ArrayList<>();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            Ingredient ingredient = Ingredient.getIngredient(item.getTextContent());
            if (ingredient != null) {
                ingredients.add(ingredient);
            }
        }
        return ingredients;
    }

    private Value getValue(Node node) {
        Value value = new Value();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            if (item.getNodeName().equals("proteins")) {
                value.setProteins(Double.valueOf(item.getTextContent()));
            } else if (item.getNodeName().equals("fats")) {
                value.setFats(Double.valueOf(item.getTextContent()));
            } else if (item.getNodeName().equals("carbohydrates")) {
                value.setCarbohydrates(Double.valueOf(item.getTextContent()));
            }
        }
        return value;
    }

}