package com.polahniuk.model.parser;

import com.polahniuk.model.candy.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which parse xml documents using SAX.
 */
public class SaxParser {

    private Logger log = LogManager.getLogger(SaxParser.class);
    private SAXParserFactory factory = SAXParserFactory.newInstance();
    private SAXParser parser = factory.newSAXParser();

    private Sweet sweet = new Sweet();
    private List<Candy> candies = new ArrayList<>();

    public SaxParser(File fileXML) throws ParserConfigurationException, SAXException, IOException {
        XMLHandler handler = new XMLHandler();
        parser.parse(fileXML, handler);
    }

    public Sweet getSweet() {
        sweet.setCandies(candies);
        return sweet;
    }

    private class XMLHandler extends DefaultHandler {

        Candy candy;
        Type type;
        Value value;

        private String element;

        @Override
        public void startDocument() throws SAXException {
            element = "";
        }

        @Override
        public void endDocument() throws SAXException {
            element = null;
            candy = null;
            type = null;
            value = null;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("candy")) {
                candy = new Candy();
            } else if (qName.equals("name")) {
                element = qName;
            } else if (qName.equals("energy")) {
                element = qName;
            } else if (qName.equals("type")) {
                type = new Type();
                String include = attributes.getValue("stuffing");
                type.setInclude(Boolean.valueOf(include));
                element = qName;
            } else if (qName.equals("ingredient")) {
                element = qName;
            } else if (qName.equals("value")) {
                value = new Value();
            } else if (qName.equals("proteins")) {
                element = qName;
            } else if (qName.equals("fats")) {
                element = qName;
            } else if (qName.equals("carbohydrates")) {
                element = qName;
            } else if (qName.equals("production")) {
                element = qName;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            element = "";
            if (qName.equals("candy")) {
                candies.add(candy);
            } else if (qName.equals("value")) {
                candy.setValue(value);
            } else if (qName.equals("type")) {
                candy.setStuffing(type);
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String s = new String(ch, start, length);

            s = s.replace("\n", "").trim();
            if (element.equals("name")) {
                candy.setName(s);
            } else if (element.equals("energy")) {
                candy.setEnergy(Integer.valueOf(s));
            } else if (element.equals("type")) {
                type.setStuffing(s);
            } else if (element.equals("ingredient")) {
                Ingredient ingredient = Ingredient.getIngredient(s);
                candy.addIngredient(ingredient);
            } else if (element.equals("proteins")) {
                value.setProteins(Double.valueOf(s));
            } else if (element.equals("fats")) {
                value.setFats(Double.valueOf(s));
            } else if (element.equals("carbohydrates")) {
                value.setCarbohydrates(Double.valueOf(s));
            } else if (element.equals("production")) {
                candy.setProduction(s);
            }
        }

    }
}
