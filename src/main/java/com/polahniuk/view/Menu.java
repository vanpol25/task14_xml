package com.polahniuk.view;

import com.polahniuk.model.*;
import com.polahniuk.model.candy.Candy;
import com.polahniuk.model.candy.Sweet;
import com.polahniuk.model.parser.DomParser;
import com.polahniuk.model.parser.SaxParser;
import com.polahniuk.model.parser.StaxParser;
import org.apache.logging.log4j.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     */
    public Menu() {
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::saxParser);
        methods.put("2", this::domParser);
        methods.put("3", this::staxParser);
        show();
    }

    /**
     * Testing DOM.
     */
    private void domParser() {
        long startPerformance;
        long endPerformance;
        File file = new File("C:\\Users\\Van_PC\\git\\task14_xml\\xml\\src\\main\\resources\\xml\\candies.xml");

        try {
            startPerformance = System.currentTimeMillis();
            DomParser dp = new DomParser(file);
            List<Candy> candies = dp.getCandies();
            Sweet sweet = new Sweet();
            sweet.setCandies(candies);
            System.out.println(sweet);
            endPerformance = System.currentTimeMillis();
            System.out.println("Performance = " + (endPerformance - startPerformance) + "millis.");
        } catch (ParserConfigurationException | IOException | SAXException e) {
            log.info(e);
        }
    }

    /**
     * Testing SAX.
     */
    private void saxParser() {
        long startPerformance;
        long endPerformance;
        try {
            File file = new File("C:\\Users\\Van_PC\\git\\task14_xml\\xml\\src\\main\\resources\\xml\\candies.xml");
            startPerformance = System.currentTimeMillis();
            SaxParser sp = new SaxParser(file);
            System.out.println(sp.getSweet());
            endPerformance = System.currentTimeMillis();
            System.out.println("Performance = " + (endPerformance - startPerformance) + "millis.");
        } catch (ParserConfigurationException | SAXException | IOException e) {
            log.info(e);
        }
    }

    /**
     * Testing StAX.
     */
    private void staxParser() {
        long startPerformance;
        long endPerformance;
        try {
            File file = new File("C:\\Users\\Van_PC\\git\\task14_xml\\xml\\src\\main\\resources\\xml\\candies.xml");
            startPerformance = System.currentTimeMillis();
            StaxParser sp = new StaxParser(file);
            Sweet sweet = new Sweet();
            List<Candy> candies = sp.getCandies().stream()
                    .sorted((a, b) -> a.getEnergy() >= b.getEnergy() ? 1 : -1)
                    .collect(Collectors.toList());
            endPerformance = System.currentTimeMillis();
            sweet.setCandies(candies);
            System.out.println(sweet);
            System.out.println("Performance = " + (endPerformance - startPerformance) + "millis.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }

    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Task 1");
        menu.put("2", "2 - Task 2");
        menu.put("3", "3 - Task 3");
        menu.put("4", "4 - Task 4");
        menu.put("5", "5 - Task 5");
        menu.put("6", "6 - Task 6");
        menu.put("7", "7 - Task 7");
        menu.put("8", "8 - Task 2.1");
        menu.put("9", "9 - Task 2.2");
        menu.put("10", "10 - Task 2.3");
        menu.put("Q", "Q - Exit");
    }

    /**
     * Show menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}